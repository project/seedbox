<?php

/*
class SeedboxTorrentfileParse {
  var $index;
  var $source;
  var $final_array;

  function handler() {
    $char = $this->source[$this->index];
    if (is_numeric($char)) return $this->handler_string();
    if ($char == 'i') {
      ++$this->index;
      return $this->handler_int();
    }
    if ($char=='l') {
      ++$this->index;
      return $this->handler_list();
    }
    if ($char=='d') {
      ++$this->index;
      return $this->handler_dictonary();
    }
    die("MAIN HANDLER: UNEXPECTED CHAR (position: $this->index): ".$char);
  }

  function handler_int() {
    $current_char='';
    $number = "";
    while (($current_char = $this->source[$this->index]) != 'e') {
      ++$this->index;
      $number .= $current_char;
    }
    ++$this->index;
    return (int) $number;
  }

  function handler_string(){
    $size ="";
    while($this->source[$this->index] != ':') {
      $size .= $this->source[$this->index];
      ++$this->index;
    }
    $i = ++$this->index;
    $this->index += $size;
    $x = substr($this->source, $i, $size);
    return $x;
  }

  function handler_list() {
    $return_list = array();
    while ($this->source[$this->index] != 'e') {
      $this->index1 = $this->index;
      $return_list[] = $this->handler();
      if ($this->index1 == $this->index) die("INFINITE LOOP IN THE LIST");
    }
    ++$this->index;
    return $return_list;
  }

  function handler_dictonary() {
    $return_dict = array();
    while ($this->source[$this->index] != 'e') {
      $this->index1 = $this->index;
      $return_dict[$this->handler_string()] = $this->handler();
      if ($this->index1 == $this->index) die("INFINITE LOOP IN THE DICTONARY");
    }
    ++$this->index;
    return $return_dict;
  }


  function parse_file($filename) {
    $this->source = file_get_contents($filename);
    $this->index = 0;
    $filesize = strlen($this->source);
    $this->final_array=array();
    while($this->index<$filesize) {
      $this->index1 = $this->index;
      $this->final_array[] =$this->handler();
      if ($this->index1 == $this->index) die("INFINITE LOOP IN THE ROOT LIST");
    }
    $this->source = '';
    return $this->final_array;
  }
}
*/

class SeedboxTorrentParse {
  private $content;
  private $index = 0;
  public $resultult = array();

  function bdecode($filepath) {
    $this->content = @file_get_contents($filepath);

    if (!$this->content) {
      $this->throwException('File does not exist!');
    } else {
      if (!isset($this->content)) {
        $this->throwException('Error opening file!');
      } else {
        $this->result = $this->processElement();
      }
    }
    unset($this->content);
	    
    return $this->result;
	}

  function __destruct() {
    unset($this->content);
    unset($this->result);
  }

  private function throwException($error = 'error') {
    $this->result = array();
    $this->result['error'] = $error;
  }

  private function processElement() {
    switch($this->content[$this->index]) {
      case 'd':
        return $this->processDict();
        break;
      case 'l':
        return $this->processList();
        break;
      case 'i':
        return $this->processInt();
        break;
      default:
        if (is_numeric($this->content[$this->index])) {
          return $this->processStr();
        } else {
          $this->throwException('Unknown BEncode element');
        }
        break;
    }
  }

  private function processDict() {
    if (!$this->checkType('d')) {
      $this->throwException();
    }

    $result = array();
    $this->index++;

    while (!$this->checkType('e')) {
      $elemkey = $this->processStr();

      switch($this->content[$this->index]) {
        case 'd':
          $result[$elemkey] = $this->processDict();
          break;
        case 'l':
          $result[$elemkey] = $this->processList();
          break;
        case 'i':
          $result[$elemkey] = $this->processInt();
          break;
        default:
          if (is_numeric($this->content[$this->index])) {
            $result[$elemkey] = $this->processStr();
          } else {
            $this->throwException('Unknown element!');
          }
          break;
      }
    }

    $this->index++;
    return $result;
  }

  private function processList() {
    if (!$this->checkType('l')) {
      $this->throwException();
    }
      

      $result = array();
      $this->index++;

      while (!$this->checkType('e'))
        $result[] = $this->processElement();

        $this->index++;
        return $result;
  }

  private function processInt() {
    if (!$this->checkType('e')) {
      $this->throwException();
    }

      $this->index++;

      $delim_pos = strpos($this->content, 'e', $this->index);
      $int = substr($this->content, $this->index, $delim_pos - $this->index);
      if (($int == '-0') || ((substr($int, 0, 1) == '0') && (strlen($int) > 1)))
        $this->throwException();

    $int = abs(intval($int));
    $this->index = $delim_pos + 1;
    return $int;
  }

  private function processStr() {
    if (!is_numeric($this->content[$this->index])) {
      $this->throwException();
    }

    $delim_pos = strpos($this->content, ':', $this->index);
    $elem_len = intval(substr($this->content, $this->index, $delim_pos - $this->index));
    $this->index = $delim_pos + 1;

    $elem_name = substr($this->content, $this->index, $elem_len);

    $this->index += $elem_len;
    return $elem_name;
  }

  private function checkType($type) {
    return ($this->content[$this->index] == $type);
  }

  function bencode($element) {
    $out = "";
    if (is_int($element)) {
      $out = 'i'.$element.'e';
    } else if (is_string($element)) {
      $out = strlen($element).':'.$element;
    } else if (is_array($element)) {
      ksort($element);
      if (is_string(key($element))) {
        $out ='d';
        foreach($element as $key => $val)
          $out .= self::bencode($key) . self::bencode($val);
          $out .= 'e';
        } else {
          $out ='l';
        foreach($element as $val)
          $out .= self::bencode($val);
          $out .= 'e';
        }
      } else {
        print("$element");
        trigger_error("unknown element type: '".
        gettype($element)."'", E_USER_ERROR);
        exit();
      }
    return $out;
  }
}


/**
 * Seedbox upload (seedboxupload://) stream wrapper class.
 *
 * Provides support for storing publicly accessible files with the Drupal file
 * interface.
 */

class SeedboxUploadStreamWrapper extends DrupalLocalStreamWrapper {
  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return variable_get('seedbox_torrentfile_upload_path', '');
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a public file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    $uri = 'seedbox/upload/' . $path;
    drupal_alter('seedbox_torrent_uri', $uri);
//    if (module_exists('seedbox_utorrent')) {
//      return url(' . '.loaded', array('absolute' => TRUE));
//    }
//    else {
      return url($uri, array('absolute' => TRUE));
//    }
  }
}

/*
 * Drupal private (private://) stream wrapper class.
 *
 * Provides support for storing privately accessible files with the Drupal file
 * interface.
 *
 * Extends DrupalPublicStreamWrapper.
 */
class SeedboxDownloadStreamWrapper extends DrupalLocalStreamWrapper {
  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return variable_get('seedbox_downloaded_path', '');
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a private file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return url('seedbox/download/' . $path, array('absolute' => TRUE));
  }
}

class SeedboxRecursiveZip extends ZipArchive {
    
  public function addDirectory($dir) { // adds directory
    if (is_dir($dir) && ($dh = opendir($dir))) {
      while (($file = readdir($dh)) !== false) {
        if (is_dir($dir . '/' . $file) && ($file != '.' && $file != '..')) {
          $this->addDirectory($dir . '/' . $file);
        }
        else {
          if (is_readable($dir . '/' . $file) && ($file != '.' && $file != '..')) {
            $this->addFile($dir . '/' . $file);
          }
        }
      }
    }
  }
}

