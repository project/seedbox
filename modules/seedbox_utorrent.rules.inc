<?php

function seedbox_utorrent_rules_action_info() {
  return array(
    'seedbox_utorrent_force_start' => array(
      'label' => t('Force Start'),
      'description' => t('Force starts a torrent'),
      'group' => t('Seedbox uTorrent'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Downloaded Node'),
        ),
      ),
//      'base' => 'seedbox_utorrent',
    ),
    'seedbox_utorrent_stop' => array(
      'label' => t('Stop'),
      'description' => t('Stops a torrent'),
      'group' => t('Seedbox uTorrent'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Downloaded Node'),
        ),
      ),
//      'base' => 'seedbox_utorrent',
    ),
    'seedbox_utorrent_delete' => array(
      'label' => t('Delete'),
      'description' => t('Deletes a torrent'),
      'group' => t('Seedbox uTorrent'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Downloaded Node'),
        ),
      ),
//      'base' => 'seedbox_utorrent',
    ),
  );
}
