<?php

function seedbox_utorrent_admin_form($form, &$form_state) {

  $form['seedbox_utorrent'] = array(
   '#title' => t('Seedbox uTorrent Settings'),
    '#type' => 'fieldset',
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['seedbox_utorrent']['seedbox_utorrent_host'] = array(
    '#type' => 'textfield',
    '#title' => t('uTorrent Host'),
    '#size'=> 30,
    '#required' => TRUE,
    '#default_value' => variable_get('seedbox_utorrent_host', 25),
    '#description' => t('Enter the hostname of the location of uTorrent.'),
  );

  $form['seedbox_utorrent']['seedbox_utorrent_port'] = array(
    '#type' => 'textfield',
    '#title' => t('uTorrent Port'),
    '#default_value' => variable_get('seedbox_utorrent_port'),
    '#size' => 30,
    '#required' => TRUE,
    '#description' => t('Enter the port of the uTorrent web interface.'),
  );

  $form['seedbox_utorrent']['seedbox_utorrent_username'] = array(
    '#type' => 'textfield',
    '#title' => t('uTorrent Username'),
    '#default_value' => variable_get('seedbox_utorrent_username'),
    '#size' => 30,
    '#required' => TRUE,
    '#description' => t('Enter the username for the administrative user allowed access to uTorrent.'),
  );
// TODO change to pw field
  $form['seedbox_utorrent']['seedbox_utorrent_password'] = array(
    '#type' => 'textfield',
    '#title' => t('uTorrent Password'),
    '#default_value' => variable_get('seedbox_utorrent_password'),
    '#size' => 30,
    '#required' => TRUE,
    '#description' => t('Enter the password for the administrative user allowed access to uTorrent.'),
  );

  $form['#validate'] = array('seedbox_utorrent_admin_form_validate');

  return system_settings_form($form);

}

function seedbox_utorrent_admin_form_validate($form, &$form_state) {
  $utorrent = new SeedboxUtorrent();
  $host = $form_state['values']['seedbox_utorrent_host'];
  $port = $form_state['values']['seedbox_utorrent_port'];
  $user = $form_state['values']['seedbox_utorrent_username'];
  $pass = $form_state['values']['seedbox_utorrent_password'];
  $http = $utorrent->correctCurl($host, $port, $user, $pass);
  switch($http) {
    case 200:
    break;
    case 400:
      $message = t('Bad request, contact the module maintainer');
      break;
    case 401:
      $message = t('Unauthorised access. Username and password must match those set in uTorrent settings');
      break;
    case 404:
      $message = t('404 error, could not connect to server. Try changing address and ensure server is running.');
    break;
    default:
      $message = t('Failed for unknown reason');
      break;
  }

  if ($message) {
    form_set_error('seedbox_utorrent', $message);
  }
}
