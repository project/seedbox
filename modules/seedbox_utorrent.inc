<?php

class SeedboxUtorrent {
  public $host = ''; //variable_get('seedbox_utorrent_host', NULL);
  public $port = ''; // variable_get('seedbox_utorrent_port', NULL);
  private $user = ''; //variable_get('seedbox_utorrent_username', NULL);
  private $password = ''; //variable_get('seedbox_utorrent_password', NULL);
  protected $token = '';

  function getList() {
    $this->token = $this->getToken();
    $action = '?list=1&token=' . $this->token;
    $results = $this->createConnection($action);
    return drupal_json_decode($results);
  }

  function getProps($hash) {
    $this->token = $this->getToken();
    $action = '?action=getprops&hash=' . $hash . '&token=' . $this->token;
    $results = $this->createConnection($action);
    return drupal_json_decode($results);
  }

  function getInfo($hash) {
    $this->token = $this->getToken();
    $action = '?action=getfiles&hash=' . $hash . '&token=' . $this->token;
    $results = $this->createConnection($action);
    return $this->parseHashResponse(drupal_json_decode($results));
  }

  function getRawInfo($hash) {
    $this->token = $this->getToken();   
    $action = '?action=getfiles&hash=' . $hash . '&token=' . $this->token;
    $results = $this->createConnection($action);
    return drupal_json_decode($results);
  }

  function forceStart($hash) {
    $this->token = $this->getToken();
    $action = '?action=forcestart&hash=' . $hash . '&token=' . $this->token;
    $results = $this->createConnection($action);
    return drupal_json_decode($results);
  }

  function start($hash) {
    $this->token = $this->getToken();
    $action = '?action=start&hash=' . $hash . '&token=' . $this->token;
    $results = $this->createConnection($action);
    return drupal_json_decode($results);
  }

  function stop($hash) {
    $this->token = $this->getToken();
    $action = '?action=stop&hash=' . $hash . '&token=' . $this->token;
    $results = $this->createConnection($action);
    return drupal_json_decode($results);
  }

  function delete($hash) {
    $this->token = $this->getToken();
    $action = '?action=removedata&hash=' . $hash . '&token=' . $this->token;
    $results = $this->createConnection($action);
    return drupal_json_decode($results);
  }

//force start

//stop

//remove
//removedata

  function getToken() {
    $results = $this->createConnection('token.html');
    preg_match("/<div[^>]*id=[\"']token[\"'][^>]*>([^<]*)<\/div>/",$results,$matches);
    $this->token = $matches[1];
    return $this->token;
  }

  function createConnection($action) {
    $ch = curl_init();
    $this->host = variable_get('seedbox_utorrent_host', NULL);
    $this->port = variable_get('seedbox_utorrent_port', NULL);
    $this->user = variable_get('seedbox_utorrent_username', NULL);
    $this->password = variable_get('seedbox_utorrent_password', NULL);
    $curl_opts = array(
      CURLOPT_FRESH_CONNECT => 1,
      CURLOPT_FORBID_REUSE => 1,
      CURLOPT_USERPWD => $this->user . ':' . $this->password,
      CURLOPT_HEADER => 0,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $this->host . ':' . $this->port . '/gui/' . $action,
      CURLOPT_SSL_VERIFYPEER => FALSE,
      CURLOPT_FOLLOWLOCATION => 1,
      CURLOPT_COOKIEFILE => file_directory_temp() . '/seedbox_utorrent.cookie',
      CURLOPT_COOKIEJAR => file_directory_temp() . '/seedbox_utorrent.cookie'
    );
    curl_setopt_array($ch, $curl_opts);
  //error handle other error codes for response
    $results = curl_exec($ch);
    return $results;
  }

  function correctCurl($host, $port, $user, $pass) {
    $ch = curl_init();
    $curl_opts = array(
      CURLOPT_FRESH_CONNECT => 1,
      CURLOPT_FORBID_REUSE => 1,
      CURLOPT_USERPWD => $user . ':' . $pass,
      CURLOPT_HEADER => 0,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $host . ':' . $port . '/gui/token.html',
      CURLOPT_SSL_VERIFYPEER => FALSE,
      CURLOPT_FOLLOWLOCATION => 1,
      CURLOPT_COOKIEFILE => file_directory_temp() . '/seedbox_utorrent.cookie',
      CURLOPT_COOKIEJAR => file_directory_temp() . '/seedbox_utorrent.cookie'
    );
    curl_setopt_array($ch, $curl_opts);
    $results = curl_exec($ch);
    return curl_getinfo($ch, CURLINFO_HTTP_CODE);;
  }

  function percentDownloaded($hash) {
    $data = $this->getInfo($hash);
    
    return $data['percent'];
  }

  function parseHashResponse($data) {
    $total = 0;
    $dl = 0;

    $info = $data['files'][1];
    foreach ($info as $file) {
      $total += $file[1];
      $dl += $file[2];
    }
  /*  HASH (string),
    [
    [ FILE NAME (string),
    FILE SIZE (integer in bytes),
    DOWNLOADED (integer in bytes),
   PRIORITY* (integer) ,
       FIRST PIECE (integer),
          NUM PIECES (integer),
                        STREAMABLE (boolean),
                        ENCODED RATE (integer),
                        DURATION (integer),
                        WIDTH (integer),
                        HEIGHT (integer),
                        STREAM ETA (integer),
                        STREAMABILITY (integer) ],

*/
    $response = array(
      'hash' => $data['files'][0],
      'total' => $total,
      'downloaded' => $dl,
      'percent' => round(($dl / $total) * 100, 2, PHP_ROUND_HALF_UP),
    );

    return $response;
  }
}
