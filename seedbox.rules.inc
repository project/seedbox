<?php

/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function seedbox_rules_event_info() {
  return array(
    'seedbox_download_complete' => array(
      'label' => t('Seedbox download complete'),
      'group' => t('Seedbox'),
      'variables' => array(
        'node' => array(
        //'label' => rules_events_node_variables(t('seedbox download complete'), TRUE),
          'label' => t('Node of the downloaded content'),
          'type' => 'node',
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info()
 */
function seedbox_rules_condition_info() {
  return array(
    'seedbox_rules_condition_private' => array(
      'label' => t('Tracker is private'),
      'parameter' => array(
        'node' => array(
            'type' => 'node', 
            'label' => t('Content')
        ),
      ),
      'group' => t('Seedbox'),
//      'base' => 'seedbox_rules_condition_private',
    ),
  );
}

/**
 * Condition: Tracker is Private
 */
function seedbox_rules_condition_private($node, $settings) {
  if ($node->field_seedbox_private['und'][0]['value'] == '1') {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
