<?php

/**
 * @file
 * Serves administration pages of seedbox.
 */

/**
 * Menu callback: Shows the unique comments settings.
 */
function seedbox_admin_form($form, &$form_state) {

  $form['seedbox_status'] = array(
    '#type' => 'radios',
    '#title' => t('Seedbox Status'),
    '#default_value' => variable_get('seedbox_status', 'Off'),
    '#options' => drupal_map_assoc(array('Off', 'Download Only', 'Download/Upload')),
    '#description' => t('Select to enable Seedbox for downloads or downloads and uploads.'),
  );

  $form['seedbox_tracker_limit'] = array(
    '#type' => 'radios',
    '#title' => t('Tracker Limitations'),
    '#default_value' => variable_get('seedbox_tracker_limit', 'Private Only'),
    '#options' => drupal_map_assoc(array('Private Only', 'Private/Public')),
    '#description' => t('Select whether to enable seedbox module for Private Trackers only or both Private and Public Trackers.'),
  );
  $form['seedbox_zip_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip file limit'),
    '#size'=> 30,
    '#default_value' => variable_get('seedbox_zip_limit', 25),
    '#description' => t('Seedbox will automatically zip nested folders and folders with files greater than the zip limit. Otherwise, seedbox will attach all downloaded files to the node to be downloaded individually.'),
  );

  $form['seedbox_torrentfile_upload_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Torrentfile Upload path'),
    '#default_value' => variable_get('seedbox_torrentfile_upload_path'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('A local file system path where torrent files may be uploaded for the torrent client to find. This directory must exist and be writable by Drupal. This directory must be relative to the Drupal installation directory and be accessible over the web.'),
    '#after_build' => array('system_check_directory'),
  );

  $form['seedbox_downloaded_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Downloaded file path'),
    '#default_value' => variable_get('seedbox_downloaded_path'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('An existing local file system path for finding downloaded files. It should be writable by Drupal and not accessible over the web.'),
    '#after_build' => array('system_check_directory'),
  );

  return system_settings_form($form);
}

